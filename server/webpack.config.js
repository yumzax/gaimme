const path = require('path');
const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const nodeExternals = require("webpack-node-externals")

module.exports = {
  entry: './src/index.ts',
  output: {
    filename: 'main.js',
    path: path.resolve(__dirname, 'dist'),
  },
  mode: 'production',
  target: `node`,
  externals: [new nodeExternals()],
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: `ts-loader`,
        exclude: /node_modules/,
      },
    ],
  },
  plugins: [
    new CopyPlugin({
      patterns: [
        {
          from: './package*.json',
          to: './',
        },
      ]
    }),
  ],
  resolve: {
    plugins: [
      new TsconfigPathsPlugin({}),
    ],
    extensions: [`.tsx`, `.ts`, `.js`],
  },
};
