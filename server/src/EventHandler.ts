import { ClientEvent } from '@common/ClientEvent'
import { ServerEvent } from '@common/ServerEvent'
import io, { Socket } from 'socket.io'
import { Server } from 'http'
import { AppConfig } from './config/AppConfig'
import { Game } from './game/Game'
import { Client } from './Client'
import { Coords } from '@common/Coords'
import { UtilParse } from '@common/UtilParse'
import { InitData } from '@common/InitData'
import { PowerUpNetwork } from '@common/PowerUpNetwork'
import { GameEventHandler } from './game/GameEventHandler'
import { PowerUpTakenNetwork } from '@common/PowerUpTakenNetwork'
import { PlayerNetwork } from '@common/PlayerNetwork'

export class EventHandler {
  protected static server: io.Server

  static init(server: Server) {
    EventHandler.server = io(server)
    EventHandler.listenEvents()
    Game.setup()

    EventHandler.broadcast(ServerEvent.SERVER_INIT)
    EventHandler.broadcast(ServerEvent.SET_PLAYERS, Game.players)
    EventHandler.broadcast(ServerEvent.EMIT_POWERUPS, Game.powerUps)

    setInterval(() => {
      EventHandler.broadcast(ServerEvent.SET_PLAYERS, Game.players)
    }, AppConfig.PING_DELAY)
  }

  static listenEvents() {
    EventHandler.server.on(ClientEvent.CONNECT, (socket: Socket) => {
      const client = new Client(socket.id, socket)

      const newPlayer = Game.createPlayer(client.id)

      const initData: InitData = {
        position: newPlayer.position,
        playerId: newPlayer.id,
        color: newPlayer.color,
        players: Game.players,
      }

      client.emit(ServerEvent.INIT, initData)
      client.emit(ServerEvent.EMIT_POWERUPS, Game.powerUps)
      EventHandler.broadcast(ServerEvent.PLAYER_JOIN, newPlayer)

      client.on(ClientEvent.SEND_PLAYER_DATA, (data: string) => {
        try {
          const playerData = UtilParse.parse(data) as PlayerNetwork
          Game.setPlayerData(playerData)
        } catch (e) {}
      })

      client.on(ClientEvent.POWER_UP_TAKEN, (data: string) => {
        try {
          const powerUp = UtilParse.parse(data) as PowerUpTakenNetwork
          GameEventHandler.handlePowerUpTaken(powerUp)
        } catch (e) {
          console.error(e)
        }
      })

      client.on(ClientEvent.SEND_PLAYER_CUSTOM_DATA, (data: string) => {
        try {
          const parsedData = UtilParse.parse(data) as any
          const name = parsedData.name
          const color = parsedData.color
          GameEventHandler.setPlayerName(socket.id, name)
          GameEventHandler.setPlayerColor(socket.id, color)
        } catch (e) {
          console.error(e)
        }
      })

      client.on(ClientEvent.DISCONNECT, () => {
        Game.removePlayer(client.id)
        EventHandler.broadcast(ServerEvent.PLAYER_LEAVE, client.id)
        console.log(`User disconnected : ${client.id}`)
      })
    })
  }

  static broadcast(eventName: ServerEvent, data: any = null) {
    try {
      const parsedData = UtilParse.stringify(data)
      EventHandler.server.emit(eventName, parsedData)
    } catch (e) {}
  }
}
