import { Coords } from '@common/Coords'
import { CommonConfig } from '@common/CommonConfig'
import { UtilRandom } from '@common/UtilRandom'
import { PowerUpTypes } from '@common/constants/PowerUpTypes'
import { Size } from '@common/Size'
import { PowerUpNetwork } from '@common/PowerUpNetwork'
import { Game } from './Game'

export class PowerUp implements PowerUpNetwork {
  public position: Coords
  public size: Size
  public id: string
  public type: PowerUpTypes
  public power: number
  public lifetime: number

  constructor(type: PowerUpTypes) {
    this.type = type
    this.id = UtilRandom.randomId()
    this.size = {
      width: CommonConfig.POWER_UP_WIDTH,
      height: CommonConfig.POWER_UP_HEIGHT,
    }
    this.position = UtilRandom.randomPosition(this.size)
    this.power = UtilRandom.randomPower()
    this.lifetime =
      CommonConfig.POWER_UP_LIFE_TIME * UtilRandom.randomPower() * 1000

    // setTimeout(() => {
    //   Game.removePowerUp(this.id)
    //   Game.powerUps.push(new PowerUp(this.type))
    // }, this.lifetime)
  }
}
