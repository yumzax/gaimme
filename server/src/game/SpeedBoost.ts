import { Coords } from '@common/Coords'
import { UtilRandom } from '@common/UtilRandom'
import { PowerUpNetwork } from '@common/PowerUpNetwork'
import { PowerUpTypes } from '@common/constants/PowerUpTypes'
import { Size } from '@common/Size'
import { CommonConfig } from '@common/CommonConfig'

export class SpeedBoost implements PowerUpNetwork {
  public position: Coords
  id: string
  type: PowerUpTypes
  size: Size
  power: number
  lifetime: number

  constructor() {
    this.size = {
      width: CommonConfig.SPEED_BOOST_WIDTH,
      height: CommonConfig.SPEED_BOOST_HEIGHT,
    }
    this.position = UtilRandom.randomPosition(this.size)
    this.type = PowerUpTypes.SPEED_BOOST
    this.id = UtilRandom.randomId()
  }
}
