export enum DOMConstants {
  BG_CANVAS_ID = "background-canvas",
  GAME_CANVAS_ID = "game-canvas",
  GUI_CANVAS_ID = "gui-canvas"
}
