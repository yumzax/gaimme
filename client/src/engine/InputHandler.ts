import { InputKeys } from './InputKeys'
import { GameRenderer } from './GameRenderer'
import { GameEventHandler } from 'src/game/core/GameEventHandler'

export class InputHandler {
  static keyState: Map<string, boolean> = new Map()

  static init() {
    for (const key in InputKeys) {
      InputHandler.keyState.set(key, false)
    }

    document.addEventListener('keydown', (e: KeyboardEvent) => {
      InputHandler.keyState.set(e.key, true)
    })

    document.addEventListener('keyup', (e: KeyboardEvent) => {
      InputHandler.keyState.set(e.key, false)
    })

    window.addEventListener('resize', (e: Event) => {
      GameRenderer.computeCanvasSize()
    })
    const form = document.getElementById('send-player-data') as HTMLFormElement
    const inputName = document.getElementById('player-name') as HTMLInputElement
    const inputColor = document.getElementById(
      'player-color'
    ) as HTMLInputElement

    if (form && inputName) {
      form.addEventListener('submit', (e) => {
        e.preventDefault()
        GameEventHandler.setMainPlayerCustomData(
          inputName.value,
          inputColor.value
        )
        e.stopPropagation()
        return false
      })
    }
  }
}
