import { SpecificDrawManager } from 'src/engine/SpecificDrawManager';
import { backgroundDrawManager } from 'src/game/core/BackgroundDrawManager';
import { gameDrawManager } from 'src/game/core/GameDrawManager';
import { guiDrawManager } from 'src/game/gui/GUIDrawManager';
import { CommonConfig } from '@common/CommonConfig';

export class GameRenderer {
  static CANVAS_WIDTH: number
  static CANVAS_HEIGHT: number
  static gameToWindowRatioWidth: number
  static gameToWindowRatioHeight: number
  static drawables: SpecificDrawManager[] = []

  static init() {
    GameRenderer.drawables = [guiDrawManager, gameDrawManager, backgroundDrawManager]
    GameRenderer.computeCanvasSize();
  }

  static computeCanvasSize() {
    const windowRatio = window.innerWidth / window.innerHeight
    const GAME_RATIO = CommonConfig.GAME_WIDTH / CommonConfig.GAME_HEIGHT

    if (windowRatio < GAME_RATIO) {
      GameRenderer.CANVAS_WIDTH = window.innerWidth
      GameRenderer.CANVAS_HEIGHT = window.innerWidth / GAME_RATIO
    } else {
      GameRenderer.CANVAS_WIDTH = window.innerHeight * GAME_RATIO
      GameRenderer.CANVAS_HEIGHT = window.innerHeight
    }

    GameRenderer.gameToWindowRatioWidth = CommonConfig.GAME_WIDTH / GameRenderer.CANVAS_WIDTH
    GameRenderer.gameToWindowRatioHeight = CommonConfig.GAME_HEIGHT / GameRenderer.CANVAS_HEIGHT

    for (const drawable of GameRenderer.drawables) {
      drawable.scaleToWindow(
        GameRenderer.CANVAS_WIDTH,
        GameRenderer.CANVAS_HEIGHT,
        1 / GameRenderer.gameToWindowRatioWidth,
        1 / GameRenderer.gameToWindowRatioHeight
      )
    }
  }

  static draw() {
    for (const drawable of GameRenderer.drawables) {
      drawable.draw();
    }
  }
}
