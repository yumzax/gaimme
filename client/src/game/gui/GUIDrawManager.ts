import { Collider } from '@common/Collider';
import { Coords } from '@common/Coords';
import { GameColors } from 'src/game/constants/GameColors';
import { GUIConstants } from 'src/game/gui/GUIConstants';
import { GameConstants } from 'src/game/constants/GameConstants';
import { Game } from '../Game';
import { SpecificDrawManager } from '../../engine/SpecificDrawManager';
import { DOMConstants } from 'src/engine/constants/DOMConstants';
import { CommonConfig } from '@common/CommonConfig';
import { Size } from '@common/Size';

interface SpeedMeterArrowSave {
  p1: Coords
  p2: Coords
  p3: Coords
}

export class GUIDrawManager extends SpecificDrawManager {
  private speedMeterArrowSave: SpeedMeterArrowSave = {
    p1: { x: -1, y: -1 }, p2: { x: -1, y: -1 }, p3: { x: -1, y: -1 }
  };
  private accelerationMeterArrowSave: SpeedMeterArrowSave = {
    p1: { x: -1, y: -1 }, p2: { x: -1, y: -1 }, p3: { x: -1, y: -1 }
  };
  private speedMeterCoords: Coords = {
    x: CommonConfig.GAME_WIDTH - GUIConstants.SPEED_METER_WIDTH - GUIConstants.SPEED_METER_MARGIN,
    y: CommonConfig.GAME_HEIGHT - GUIConstants.SPEED_METER_HEIGHT - GUIConstants.SPEED_METER_MARGIN,
  };
  private arrowBaseCoords: Coords = {
    x: this.speedMeterCoords.x + GUIConstants.SPEED_METER_WIDTH / 2,
    y: this.speedMeterCoords.y + GUIConstants.SPEED_METER_HEIGHT,
  }
  refreshRate: number = GUIConstants.REFRESH_RATE;

  constructor() {
    super(DOMConstants.GUI_CANVAS_ID, true);
  }

  draw() {
    this.handleRefresh(this.drawGUI);
  }

  drawGUI = () => {
    this.ctx.clearRect(0, 0, CommonConfig.GAME_WIDTH, CommonConfig.GAME_HEIGHT)
    this.drawScores();
    this.drawSpeedMeter();
    this.drawAccelerationMeter();
  }

  private drawSpeedMeter() {
    let alpha = GUIConstants.ALPHA_NO_COLLISION
    const speedMeterSize: Size = {
      width: GUIConstants.SPEED_METER_WIDTH, height: GUIConstants.SPEED_METER_HEIGHT
    }
    const humanPlayer = Game.getHumanPlayer()

    if (Collider.areColliding(this.speedMeterCoords, humanPlayer.position, speedMeterSize, humanPlayer.size)) {
      alpha = GUIConstants.ALPHA_COLLISION
    }
    this.ctx.fillStyle = GameColors.WHITE;
    this.drawRect(this.speedMeterCoords, speedMeterSize, alpha);
    const currentPlayerVelocity = humanPlayer.getNormalVelocity()
    const maxVelocityRatio = currentPlayerVelocity / GameConstants.MAX_PLAYER_VELOCITY;

    const theta = (1 - maxVelocityRatio) * Math.PI + Math.PI / 2
    const p1 = { x: this.arrowBaseCoords.x, y: this.arrowBaseCoords.y } as Coords;

    const p2 = {
      x: (this.arrowBaseCoords.x) + GUIConstants.RADIUS * Math.sin(theta),
      y: (this.arrowBaseCoords.y) + GUIConstants.RADIUS * Math.cos(theta),
    }

    const thetaMax = (1 - humanPlayer.currentMaxVelocity / GameConstants.MAX_PLAYER_VELOCITY) * Math.PI + Math.PI / 2
    const p3 = {
      x: (this.arrowBaseCoords.x) + GUIConstants.RADIUS * Math.sin(thetaMax),
      y: (this.arrowBaseCoords.y) + GUIConstants.RADIUS * Math.cos(thetaMax),
    }

    this.speedMeterArrowSave.p1 = p1;
    this.speedMeterArrowSave.p2 = p2;
    this.speedMeterArrowSave.p3 = p3;

    this.ctx.fillStyle = GameColors.SPEED_METER_MAX_FILL;
    this.drawRect(
      { x: this.speedMeterCoords.x, y: this.speedMeterCoords.y },
      { width: this.speedMeterArrowSave.p3.x - this.speedMeterCoords.x, height: GUIConstants.SPEED_METER_HEIGHT },
      alpha
    );

    this.ctx.strokeStyle = GameColors.SPEED_METER_ARROW
    this.drawArrow(this.speedMeterArrowSave.p1, this.speedMeterArrowSave.p2, alpha);
  }

  private drawAccelerationMeter() {
    let alpha = GUIConstants.ALPHA_NO_COLLISION
    const speedMeterSize: Size = {
      width: GUIConstants.SPEED_METER_WIDTH, height: GUIConstants.SPEED_METER_HEIGHT
    }
    const humanPlayer = Game.getHumanPlayer()

    if (Collider.areColliding(this.speedMeterCoords, humanPlayer.position, speedMeterSize, humanPlayer.size)) {
      alpha = GUIConstants.ALPHA_COLLISION
    }
    this.ctx.fillStyle = GameColors.WHITE;
    this.drawRect(this.speedMeterCoords, speedMeterSize, alpha);
    const currentPlayerAcceleration = humanPlayer.getNormalAcceleration()
    const maxAccelerationRatio = currentPlayerAcceleration / Math.sqrt(2 * Math.pow(GameConstants.MAX_PLAYER_ACCELERATION, 2));

    const theta = (1 - maxAccelerationRatio) * Math.PI + Math.PI / 2
    const p1 = { x: this.arrowBaseCoords.x, y: this.arrowBaseCoords.y } as Coords;

    const p2 = {
      x: (this.arrowBaseCoords.x) + GUIConstants.RADIUS * Math.sin(theta),
      y: (this.arrowBaseCoords.y) + GUIConstants.RADIUS * Math.cos(theta),
    }

    const thetaMax = (1 - humanPlayer.currentMaxAcceleration / GameConstants.MAX_PLAYER_ACCELERATION) * Math.PI + Math.PI / 2
    const p3 = {
      x: (this.arrowBaseCoords.x) + GUIConstants.RADIUS * Math.sin(thetaMax),
      y: (this.arrowBaseCoords.y) + GUIConstants.RADIUS * Math.cos(thetaMax),
    }

    this.accelerationMeterArrowSave.p1 = p1;
    this.accelerationMeterArrowSave.p2 = p2;
    this.accelerationMeterArrowSave.p3 = p3;

    this.ctx.strokeStyle = GameColors.ACCELERATION_METER_ARROW
    this.drawArrow(this.accelerationMeterArrowSave.p1, this.accelerationMeterArrowSave.p2, alpha);
  }

  drawScores() {
    const scores = Game.scores;
    const mainPlayer = Game.getHumanPlayer();
    const OFFSET_X = 10;
    const OFFSET_Y = 17;
    let index = 1;

    for (const score of scores) {
      this.ctx.fillStyle = score.color;

      if (score.playerId !== mainPlayer.id) {
        const playerName = score.playerName;
        const playerScore = score.score;
        this.ctx.font = "8px Trebuchet MS";
        this.ctx.fillText(`${playerName} : ${playerScore}`, OFFSET_X, index * OFFSET_Y)
      } else {
        this.ctx.font = "10px Comic Sans MS";
        this.ctx.fillText(`${mainPlayer.name} : ${mainPlayer.score}`, OFFSET_X, index * OFFSET_Y)
      }
      index++
    }
  }

}

export const guiDrawManager = new GUIDrawManager();
