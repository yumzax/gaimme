import { Coords } from '@common/Coords'
import { Vector2D } from '@common/Vector2D'
import { Alterations } from 'src/game/constants/Alterations'
import { Directions } from 'src/game/constants/Directions'
import { GameConstants } from 'src/game/constants/GameConstants'
import { Movable } from './core/Movable'
import { Player } from './core/Player'
import { Animation } from './core/Animation'
import { gameDrawManager as drawManager } from './core/GameDrawManager'
import { GUIConstants } from './gui/GUIConstants'
import { UtilRandom } from '@common/UtilRandom'

export class HumanPlayer extends Player implements Movable {
  public alteration: string
  public animations: Animation[] = []

  get isSpedUp(): boolean {
    return (
      this.currentMaxAcceleration >
      GameConstants.INITIAL_MAX_PLAYER_ACCELERATION
    )
  }

  getNormalVelocity() {
    return Math.sqrt(
      Math.pow(this.velocity.x, 2) + Math.pow(this.velocity.y, 2)
    )
  }

  getNormalAcceleration() {
    return Math.sqrt(
      Math.pow(this.acceleration.x, 2) + Math.pow(this.acceleration.y, 2)
    )
  }

  constructor(id: string, position: Coords, color: string) {
    super(id, position, color, true)
    this.alteration = Alterations.NO_ALTERATION
  }

  move(directions: Directions[]): void {
    for (const direction of directions) {
      const accelerationPower = GameConstants.ACCELERATION_POWER
      this._handleBrake(direction, accelerationPower)
      this._handleAcceleration(direction, accelerationPower)
    }

    if (
      !directions.includes(Directions.LEFT) &&
      !directions.includes(Directions.RIGHT)
    ) {
      this.acceleration.x = 0
    }
    if (
      !directions.includes(Directions.UP) &&
      !directions.includes(Directions.DOWN)
    ) {
      this.acceleration.y = 0
    }
  }

  draw() {
    if (UtilRandom.isLight(this.color)) {
      drawManager.ctx.strokeStyle = 'black'
      drawManager.ctx.lineWidth = 2
    } else {
      drawManager.ctx.strokeStyle = '#CCCCCC'
      drawManager.ctx.lineWidth = 2
    }

    const textPosition: Coords = {
      x: this.center.x,
      y: this.position.y - GUIConstants.NAME_OFFSET,
    }
    const text = this.name.slice(0, 25)
    drawManager.ctx.font = '12px Trebuchet MS'
    drawManager.strokeText(textPosition, text)
    drawManager.drawText(textPosition, text, 0.8)
    drawManager.ctx.lineWidth = 1.5
    drawManager.drawRect(this.position, this.size)
    drawManager.strokeRect(this.position, this.size)
  }

  _handleAcceleration(direction: Directions, accelerationPower: number) {
    switch (direction) {
      case Directions.UP:
        if (
          this.acceleration.y - accelerationPower >=
          -this.currentMaxAcceleration
        ) {
          this.acceleration.y -= accelerationPower
        } else {
          this.acceleration.y = -this.currentMaxAcceleration
        }
        break
      case Directions.DOWN:
        if (
          this.acceleration.y + accelerationPower <=
          this.currentMaxAcceleration
        ) {
          this.acceleration.y += accelerationPower
        } else {
          this.acceleration.y = this.currentMaxAcceleration
        }
        break
      case Directions.LEFT:
        if (
          this.acceleration.x - accelerationPower >=
          -this.currentMaxAcceleration
        ) {
          this.acceleration.x -= accelerationPower
        } else {
          this.acceleration.x = -this.currentMaxAcceleration
        }
        break
      case Directions.RIGHT:
        if (
          this.acceleration.x + accelerationPower <=
          this.currentMaxAcceleration
        ) {
          this.acceleration.x += accelerationPower
        } else {
          this.acceleration.x = this.currentMaxAcceleration
        }
        break
    }
  }

  _handleBrake(direction: Directions, accelerationPower: number) {
    switch (direction) {
      case Directions.UP:
        if (this.velocity.y > 0) {
          this.acceleration.y = 0
          if (
            this.velocity.y - accelerationPower * GameConstants.BRAKE_POWER <
            0
          ) {
            this.velocity.y = 0
          } else {
            this.velocity.y -= accelerationPower * GameConstants.BRAKE_POWER
          }
        }
        break
      case Directions.DOWN:
        if (this.velocity.y < 0) {
          this.acceleration.y = 0
          if (
            this.velocity.y + accelerationPower * GameConstants.BRAKE_POWER >
            0
          ) {
            this.velocity.y = 0
          } else {
            this.velocity.y += accelerationPower * GameConstants.BRAKE_POWER
          }
        }
        break
      case Directions.LEFT:
        if (this.velocity.x > 0) {
          this.acceleration.x = 0
          if (
            this.velocity.x - accelerationPower * GameConstants.BRAKE_POWER <
            0
          ) {
            this.velocity.x = 0
          } else {
            this.velocity.x -= accelerationPower * GameConstants.BRAKE_POWER
          }
        }
        break
      case Directions.RIGHT:
        if (this.velocity.x < 0) {
          this.acceleration.x = 0
          if (
            this.velocity.x + accelerationPower * GameConstants.BRAKE_POWER >
            0
          ) {
            this.velocity.x = 0
          } else {
            this.velocity.x += accelerationPower * GameConstants.BRAKE_POWER
          }
        }
        break
    }
  }
}
