import { DrawableGameObject } from './DrawableGameObject'

export abstract class Animation {
  protected enabled = true

  constructor(protected speed: number = 1) {}

  abstract animate(gameObject: DrawableGameObject): void

  enable(): void {
    this.enabled = true
  }

  disable(): void {
    this.enabled = false
  }
}
