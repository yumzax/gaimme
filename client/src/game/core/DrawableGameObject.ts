import { Coords } from '@common/Coords';
import { Size } from '@common/Size';
import { gameDrawManager } from './GameDrawManager';
import { GameObject } from './GameObject';
import { Animation } from './Animation';
import { gameDrawManager as drawManager } from './GameDrawManager';

export abstract class DrawableGameObject extends GameObject {
  abstract color: string
  animations: Animation[] = []
  exists = true

  constructor(position: Coords, size: Size, id = "", foreground = false) {
    super(position, size, id)
    gameDrawManager.addToQueue(this, foreground);
  }

  handleDraw(): void {
    drawManager.ctx.save()
    if (this.animations.length > 0) {
      for (const animation of this.animations) {
        animation.animate(this)
      }
    } else {
      drawManager.ctx.fillStyle = this.color;
    }

    this.draw()
    drawManager.ctx.restore()
  }

  protected abstract draw(): void

  destroy() {
    this.exists = false
    gameDrawManager.removeFromQueue(this.id)
  }
}
