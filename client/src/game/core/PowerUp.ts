import { Collider } from '@common/Collider'
import { CommonConfig } from '@common/CommonConfig'
import { PowerUpTypes } from '@common/constants/PowerUpTypes'
import { Coords } from '@common/Coords'
import { Size } from '@common/Size'
import { HumanPlayer } from '../HumanPlayer'
import { DrawableGameObject } from './DrawableGameObject'
import { gameDrawManager } from './GameDrawManager'
import { Player } from './Player'

export abstract class PowerUp extends DrawableGameObject {
  abstract type: PowerUpTypes
  power: number

  constructor(
    id: string,
    position: Coords,
    size: Size = {
      width: CommonConfig.POWER_UP_WIDTH,
      height: CommonConfig.POWER_UP_HEIGHT,
    },
    power: number
  ) {
    super(position, size)
    this.id = id
    this.power = power
  }

  isTaken(taker: Player): boolean {
    return (
      this.exists &&
      Collider.areColliding(
        taker.position,
        this.position,
        taker.size,
        this.size
      )
    )
  }

  destroy() {
    this.exists = false
    gameDrawManager.removeFromQueue(this.id)
  }

  triggerEffect(player: HumanPlayer): void {
    this.handleEffect(player)
    this.destroy()
  }

  protected abstract handleEffect(player: HumanPlayer): void
}
