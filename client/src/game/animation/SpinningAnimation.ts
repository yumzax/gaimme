import { GameConstants } from '../constants/GameConstants'
import { Animation } from '../core/Animation'
import { DrawableGameObject } from '../core/DrawableGameObject'
import { gameDrawManager } from '../core/GameDrawManager'

export class SpinningAnimation extends Animation {
  private static MAX_FRAMES = GameConstants.DRAW_REFRESH_RATE * 4
  private state: number = 0

  get speedRate(): number {
    return SpinningAnimation.MAX_FRAMES / this.speed
  }

  animate(player: DrawableGameObject): void {
    if (!this.enabled) return

    const playerCenter = {
      x: player.position.x + player.size.width / 2,
      y: player.position.y + player.size.height / 2,
    }

    gameDrawManager.ctx.translate(playerCenter.x, playerCenter.y)
    gameDrawManager.ctx.rotate((2 * Math.PI * this.state) / this.speedRate)
    gameDrawManager.ctx.fillStyle = player.color
    this.state = (this.state + 1) % this.speedRate
    gameDrawManager.ctx.translate(-playerCenter.x, -playerCenter.y)
  }
}
