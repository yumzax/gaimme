import { Coords } from '@common/Coords'
import { GameRenderer } from '../engine/GameRenderer'
import { GameConstants } from './constants/GameConstants'
import { HumanPlayer } from './HumanPlayer'
import { RemotePlayer } from './RemotePlayer'
import { GameEventHandler } from './core/GameEventHandler'
import { PowerUp } from './core/PowerUp'
import { PlayerNetwork } from '@common/PlayerNetwork'
import { Player } from './core/Player'

interface Score {
  playerId: string
  playerName: string
  score: number
  color: string
}

export class Game {
  static humanPlayer: HumanPlayer
  static remotePlayers: RemotePlayer[] = []
  static powerUps: PowerUp[] = []
  static scores: Score[] = []
  static initialized = false

  static get players(): Player[] {
    return [Game.humanPlayer, ...Game.remotePlayers]
  }

  static run() {
    window.setInterval(GameRenderer.draw, GameConstants.DRAW_REFRESH_RATE)
    window.setInterval(Game.gameLoop, GameConstants.GAME_LOOP_REFRESH_RATE)
    console.log('Game running!')
  }

  static gameLoop() {
    GameEventHandler.handleMove()
    GameEventHandler.checkPowerUps()
    GameEventHandler.checkPlayerBump()
    GameEventHandler.checkOffLimits()
    GameEventHandler.sendPlayerData()
  }

  static updateRemotePlayers(players: PlayerNetwork[]) {
    for (const player of Game.players) {
      const remotePlayer = players.find((p) => p.id === player.id)
      if (remotePlayer) {
        if (remotePlayer.id !== Game.getHumanPlayer().id) {
          player.position = remotePlayer.position
          player.size = remotePlayer.size
          player.velocity = remotePlayer.velocity
        }
        player.score = remotePlayer.score
        player.color = remotePlayer.color
        player.name = remotePlayer.name
      }
    }
    Game.sortScores()
  }

  static sortScores() {
    let scores: Score[] = []
    for (const player of Game.players) {
      scores.push({
        score: player.score,
        playerId: player.id,
        playerName: player.name,
        color: player.color,
      })
    }
    scores = scores.sort((a, b) => b.score - a.score)
    Game.scores = scores
  }

  static updatePowerUps(powerUps: PowerUp[]) {
    for (const powerUp of powerUps) {
      const powerUpToUpdate = Game.powerUps.find((p) => p.id === powerUp.id)
      if (powerUpToUpdate) {
        powerUp.position = powerUpToUpdate.position
      }
    }
  }

  static addPowerUp(powerUp: PowerUp) {
    Game.powerUps.push(powerUp)
  }

  static createHumanPlayer(id: string, color: string, position: Coords) {
    Game.humanPlayer = new HumanPlayer(id, position, color)
  }

  static getHumanPlayer() {
    return Game.humanPlayer
  }
}
