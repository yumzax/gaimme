import { Collider } from '@common/Collider'
import { UtilRandom } from '@common/UtilRandom'
import io from 'socket.io-client'
import { GameRenderer } from './engine/GameRenderer'
import { InputHandler } from './engine/InputHandler'
import { NetworkEventHandler } from './engine/NetworkEventHandler'
import { CommonConfig } from '@common/CommonConfig'

export class App {
  constructor() {
    const port = process.env.PORT || 80
    const host = process.env.HOST || `gaimme.herokuapp.com`
    const server = io(`http://${host}:${port}`)
    NetworkEventHandler.init(server)
    InputHandler.init()
    GameRenderer.init()
    UtilRandom.init(CommonConfig.GAME_WIDTH, CommonConfig.GAME_HEIGHT)
    console.log('App started!')
  }
}
