import { PositionedItem } from './PositionedItem'
import { Coords } from './Coords'

export interface InitData {
  playerId: string
  color: string
  position: Coords
  players: PositionedItem[]
}
