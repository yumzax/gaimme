import { Coords } from './Coords'
import { PowerUpTypes } from './constants/PowerUpTypes'
import { Size } from './Size'

export interface PowerUpNetwork {
  id: string
  type: PowerUpTypes
  position: Coords
  size: Size
  power: number
  lifetime: number
}
