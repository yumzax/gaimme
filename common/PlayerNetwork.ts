import { Size } from "./Size";
import { Coords } from "./Coords";
import { Vector2D } from "./Vector2D";

export interface PlayerNetwork {
  id: string,
  position: Coords,
  size: Size,
  score: number,
  color: string,
  velocity: Vector2D,
  name: string
}
